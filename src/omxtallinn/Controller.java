package omxtallinn;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static java.nio.charset.StandardCharsets.UTF_16;
import static org.apache.commons.csv.CSVFormat.TDF;

public class Controller implements Initializable { //controller on kasutajaliidese juht

  @FXML
  ComboBox<Company> stocksSelect; //viide komboboxile

  @FXML
  LineChart chart; //viide chartile


  @FXML
  CategoryAxis xAxes; //viide x teljele , kuup2ev

  @FXML
  NumberAxis yAxes; //viide y teljele , hinnale

  @Override
  public void initialize(URL location, ResourceBundle resources) { //pannakse k2ima kui kasutajaliidest joonistatakse
    Company[] companies = new Company[]{
        new Company("Apranga" , "LT0000102337"),
        new Company("Arco Vara" , "EE3100034653"),
        new Company("Baltika" , "EE3100003609"),
        new Company("Ekspress Grupp" , "EE3100016965"),
        new Company("Energijos Skirstymo Operatorius" , "LT0000130023"),
        new Company("Grindeks" , "LV0000100659"),
        new Company("Grigeo Grigiškės" , "LT0000102030"),
        new Company("Harju Elekter" , "EE3100004250"),
        new Company("Klaipėdos nafta" , "LT0000111650"),
        new Company("LHV Group" , "EE3100073644"),
        new Company("Linas Agro Group" , "LT0000128092"),
        new Company("Lietuvos energijos gamyba" , "LT0000128571"),
        new Company("Latvijas kuģniecība" , "LV0000101103"),
        new Company("Merko Ehitus" , "EE3100098328"),
        new Company("Nordecon" , "EE3100039496"),
        new Company("Olympic Entertainment Group" , "EE3100084021"),
        new Company("Olainfarm" , "LV0000100501"),
        new Company("PRFoods" , "EE3100101031"),
        new Company("Panevėžio statybos trestas" , "LT0000101446"),
        new Company("Pieno žvaigždės" , "LT0000111676"),
        new Company("Rokiškio sūris" , "LT0000100372"),
        new Company("Šiaulių bankas" , "LT0000102253"),
        new Company("SAF Tehnika" , "LV0000101129"),
        new Company("Silvano Fashion Group" , "EE3100001751"),
        new Company("Skano Group" , "EE3100092503"),
        new Company("Tallink Grupp" , "EE3100004466"),
        new Company("TEO LT" , "LT0000123911"),
        new Company("Tallinna Kaubamaja Grupp" , "EE0000001105"),
        new Company("Tallinna Vesi" , "EE3100026436"),
        new Company("Utenos trikotažas" , "LT0000109324"),
        new Company("Vilkyškių pieninė" , "LT0000127508")
    };
    stocksSelect.getItems().addAll(companies); //lisab need komboboxi elementideks , kasutajaliidese (ui.fxmL) stockSelect itemiteks
  }


  public void valisinAktsia_JoonistaGraafik() throws IOException { //kui controlleris muuta comboboxi v22rtust pannakse see meetod k2ima
    Company stock = stocksSelect.getValue(); //kysib comboboxi k2est valitud ettev6tte objekti ehk milline valiti

    XYChart.Series graafikuAndmestik = new XYChart.Series(); //graafiku andmestiku komponent , mis hoiab 1 graafiku joone andmeid ( kuup2ev ja hind) , Series - andmetyyp
    graafikuAndmestik.setName(stock.name);  //andmestiku nimi all (legend)
    chart.getData().clear();  //kustutab eelmise graafiku 2ra

    List hinnad = laeAktsiaHinnad(stock.ISIN); //kutsub v2lja laeAktsiaHinnad meetodi , mis annab tagasi viimase kuue kuu b8rsi hinnad(saab ette ISN koodi)
    double minPrice = 999999;
    double maxPrice = 0;

    for (int i = 0; i < hinnad.size(); i = i + (hinnad.size() / 30)) { //tsykkel k2iakse l2bi umbes 30 korda ,igat kuuendat p2eva , quote.size on umbes 180 p2eva , iga umbes 6 p2eva tagant tehakse graaafikule punkt
      Quote hind = (Quote) hinnad.get(i);
      graafikuAndmestik.getData().add(new XYChart.Data(hind.date, hind.lastClosePrice)); //lisab andmed graafikule (iga kuuenda p2eva)

      if (hind.lastClosePrice < minPrice) { //leiab minimum hinna kolmekymnest mida tsykkel l2bi k2ib
          minPrice = hind.lastClosePrice;
      }

      if (hind.lastClosePrice > maxPrice) {
        maxPrice = hind.lastClosePrice;
      }
    }

    yAxes.setLowerBound(minPrice - (minPrice * 0.1)); //graafiku madalaim punkt
    yAxes.setUpperBound(maxPrice * 1.1);  //graafiku k6rgeim punkt
    yAxes.setTickUnit((maxPrice - minPrice) / 10); //graafiku samm

    chart.getData().add(graafikuAndmestik); //lisab andmestiku graafikule, t2nu sellele joonistakse l6puks v2lja ui.fxml
    //graafikuAndmestik.getNode().setStyle("-fx-stroke: blue;");

  }

  List laeAktsiaHinnad(String companyCode) throws IOException { //see meetod toob internetist hinnad
    String aadress ="http://www.nasdaqbaltic.com/market/?instrument=" + companyCode +"&list=2&pg=details&tab=historical&lang=en&currency=0&downloadcsv=0&date=&period=6month&pg=details&pg2=equity&downloadcsv=1&csv_style=baltic";
    URL hindadeURL = new URL(aadress);
    CSVParser csvlugeja = CSVParser.parse(hindadeURL, UTF_16, TDF.withFirstRecordAsHeader().withIgnoreEmptyLines()); //parsel laeb sellelt aadressilt CSV faili ,TDF. ytleb et esimene rida csv failist on header , ja ignoreerime tyhjasid ridu kui peaks olema

    List hinnad = new ArrayList<>(); //teeme uue list tyypi muutuja
    for (CSVRecord rida : csvlugeja) { //k2ib l2bi k6iki CSV ridu
      Quote hind = new Quote(); //iga csv rea kohta teeb uue quote objekti
      hind.date = rida.get(0); //loeme v2lja esimese tulba ehk kuup2eva
      String sulgemishind = rida.get("Close").replaceAll(",", "."); //loeb reast csv faili reast v2lja siukese headeri nagu Close ja asendab k6ik komad punktidega
      hind.lastClosePrice = Double.valueOf(sulgemishind); //teeb sulgemishinna stringist   double  ja omistame hind.lastClosePricele
      hinnad.add(hind); //lisab hinna hindade listi
    }
    return hinnad;

    // /parsib/ loeme  CSV kujul stringist v2lja quote objektid
    //C:\Users\Rene\IdeaProjects\omxtallinn\detail_EEG1T_20170105_2136.csv
  }

  /* public List<Quote> laeAktsiaHinnad(String companyCode) throws IOException {
    String aadress = "C:\\Users\\Rene\\IdeaProjects\\omxtallinn\\detail_EEG1T_20170105_2136.csv";

    File file = new File(aadress);
    FileInputStream hindadestream = new FileInputStream(file);



    //teeb URL tyypi objekti ja avab selle peal Streami ,
    String fileContent = IOUtils.toString(hindadestream, UTF_16.toString());// teeb streamist stringi
    return parse(fileContent); //parsib/ loeme  CSV kujul stringist v2lja quote objektid
  }
*/

}
