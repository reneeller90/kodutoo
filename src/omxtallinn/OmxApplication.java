package omxtallinn;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;


public class OmxApplication extends Application { 
  @Override
  public void start(Stage stage) throws Exception {
    stage.setTitle("OMX Viewer"); //stage on k6ige v2limine aken , m22rab nime

    URL uiFile = getClass().getResource("ui.fxml"); //kysib viida ui.fxml failile, laeb m2lust failinime j2rgi viida

    Parent kasutajaliides = FXMLLoader.load(uiFile);  //parent - kasutajaliidese tyypi objekt , luuakse kasutajaliidese objekt ui.fxml p6hjal
    stage.setScene(new Scene(kasutajaliides, 800, 500)); //800 ja 500 on suurus , paneme kasutajaliidese stage peale ,
    // Scene on kasutajaliidese objekt , konteiner
    stage.show(); //muudab selle stage n2htavaks
  }

  public static void main(String[] args) throws IOException {
    launch();  //launch paneb kaima JavaFX rakenduse , tuleb JavaFX Application klassist ,kasutajaliidesega rakendus ,
    // see oma korda kaivitab meetodi start

  }
}
