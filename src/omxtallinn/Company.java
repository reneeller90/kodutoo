package omxtallinn;

import java.util.Arrays;  //java enda kood , mida l2heb vaja meetodi Arrays asList meetodi kasutamiseks
import java.util.List;   //java enda kood , saab Listi tagastada  ,andmetyyp List

public class Company {
  public String name;
  public String ISIN;

  public Company(String name, String ISIN) {
    this.name = name;
    this.ISIN = ISIN;
  }


  @Override
  public String toString() {
    return name;
  }
}
